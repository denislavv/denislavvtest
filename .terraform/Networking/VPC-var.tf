# VPC variables

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "private_cirds-A" {
  type = "list"
  default = [
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

variable "private_cirds-B" {
  type = "list"
  default = [
    "10.0.5.0/24",
    "10.0.6.0/24"
  ]
}