# Choose Provider
provider "aws"
{
  region = "eu-central-1"
}

# Change the location of the terraform state to use already created bucket
terraform
{
  backend "s3"
  {
    bucket = "terraformtesthc"
    key    = "myapp/dev/terraform.tfstate"
    region = "eu-central-1"
  }
}

# VPC creation
resource "aws_vpc" "terraformtesthc"
{
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags
  {
    Name = "terraformtesthc"
  }
}

# Creation of role to assigne to policy for FlowLogs and activation of FlowLogs
//noinspection MissingProperty
resource "aws_flow_log" "FlowLogs" {
  iam_role_arn    = "${aws_iam_role.FlowLogs.arn}"
  log_destination = "${aws_cloudwatch_log_group.FlowLogs.arn}"
  traffic_type    = "ALL"
  vpc_id          = "${aws_vpc.terraformtesthc.id}"
  #log_group_name = "FlowLogs"
}

resource "aws_cloudwatch_log_group" "FlowLogs" {
  name = "FlowLogs"
}

resource "aws_iam_role" "FlowLogs" {
  name = "FlowLogs"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "FlowLogs" {
  name = "FlowLogs"
  role = "${aws_iam_role.FlowLogs.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# Internet Gateway
resource "aws_internet_gateway" "terraformtesthc_internetgateway" {
  vpc_id = "${aws_vpc.terraformtesthc.id}"

  tags {
    Name  = "tf_igw"
  }
}

# Route Tables
resource "aws_route_table" "RouteT_Public_Subnet_LoadBalancer_AZ-A" {
  vpc_id = "${aws_vpc.terraformtesthc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.terraformtesthc_internetgateway.id}"
  }
  tags {
    Name  = "public_AZ-A"
  }
}

resource "aws_default_route_table" "RouteT_Private_Subnet_Applications_AZ-A" {
  default_route_table_id = "{aws_vpc.terraformtesthc.default_route_table.id}"
  tags {
    Name = "Private_Subnet_Application_AZ-A"
  }
}


# Creation Subnets for the VPC in AZ-A
# Load Balancer Zubnet
resource "aws_subnet" "Public_Subnet_LoadBalancer" {
  vpc_id     = "${aws_vpc.terraformtesthc.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-central-1a"

  tags {
    Name = "Public_Subnet_LoadBalancer"
  }
}

# Private Sbunets-A
resource "aws_subnet" "Private_Subnets_AZ-A" {
  count = 2
  vpc_id  = "${aws_vpc.terraformtesthc.id}"
  cidr_block = "${var.private_cirds-A[count.index]}"
  availability_zone = "eu-central-1a"

  tags {
    Name = "Private_Subnet_AZ-A${count.index + 1}"
  }
}


# Creation Subnets for the VPC in AZ-B
# Load Balancer Zubnet
resource "aws_subnet" "Public_Subnet_LoadBalancer_AZ-B" {
  vpc_id     = "${aws_vpc.terraformtesthc.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "eu-central-1b"

  tags {
    Name = "Public_Subnet_LoadBalancer_AZ-B"
  }
}

# Private Subnets-B
resource "aws_subnet" "Private_Subnet_Applications_AZ-B" {
  count = 2
  vpc_id     = "${aws_vpc.terraformtesthc.id}"
  cidr_block = "${var.private_cirds-B[count.index]}"
  availability_zone = "eu-central-1b"

  tags {
    Name = "Private_Subnet_AZ-B${count.index + 1}"
  }
}
